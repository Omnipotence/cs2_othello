I did this assignment solo. 

I first wanted to make the AI work at all. I figured looping over the board and making the
first valid move I found was a good starting point. So I looked at the Board class and 
saw that it had the methods I needed to do that. I wrote up that code and tested it - save
for a few syntax errors, it worked on the first try.

Next, I wanted to beat SimplePlayer with a basic heuristic. I wrote a method that would get 
the score of a possible move by making a copy of the board, testing a potential move, and 
returning the player score - opponent score that would result from making that move. I 
edited my loop over the board so that instead of making the first move it found, it would keep
track of the best valid move found so far, then do the best move at the end of the loop.

This is where the trouble started. Everything seemed suddenly not to work. I eventually fixed 
some of the bugs (for example, I found that somehow there were all sorts of memory issues 
unless I did a make clean all every time I ran my code.) I tried various ways of doing the 
same thing, such as keeping track of the x and y coordinates of the best move instead of the 
move itself. Nothing seemed to work - the code would hang after about 12 moves, the same place,
every time. By use of fprintf, I discovered that my code was hanging while executing the Java
wrapper, not while executing my code. Problematic to say the least.

I asked several other people for help, but they were not able to see what was wrong with my
code. I have left the code that would improve the AI, commented out. I have no idea why it 
does not work. I regret having to turn in the project like this, as coding a better AI would
have been really fun.