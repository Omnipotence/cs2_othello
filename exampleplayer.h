#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();

	Board* board;
	Side mySide;
	Side opponentsSide;
	Move* m;
	//int bestScore;
	//int bestX;
	//int bestY;
	//Move* bestMove;

    
    Move *doMove(Move *opponentsMove, int msLeft);
	//int getScore(Move *testMove);
};

#endif
